package com.thanawat.affirmations.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class Affirmations(@StringRes val stringResurceId: Int,@DrawableRes val imageResourceId: Int)
